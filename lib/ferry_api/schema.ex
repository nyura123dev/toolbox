defmodule FerryApi.Schema do
  use Absinthe.Schema

  # Types
  # ------------------------------------------------------------

  import_types(Absinthe.Plug.Types)
  import_types(FerryApi.Schema.ProfileTypes)

  import_types(FerryApi.Schema.SessionType)
  import_types(FerryApi.Schema.Session)

  # Queries
  # ------------------------------------------------------------

  query do
    @desc "Health check"
    field :health_check, :string do
      resolve(fn _parent, _args, _resolution ->
        {:ok, "ok"}
      end)
    end

    import_fields(:group_queries)
    import_fields(:session_queries)
  end

  # Mutuations
  # ------------------------------------------------------------

  mutation do
    import_fields(:group_mutations)
  end
end
