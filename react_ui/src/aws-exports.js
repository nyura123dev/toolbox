const cognitoConfig = {
  aws_project_region: 'us-east-2',
  aws_cognito_region: 'us-east-2',
  aws_user_pools_id: 'us-east-2_RAorkTxmF',
  aws_user_pools_web_client_id: '72sl60728galup6u0u833d8i8q',
  oauth: {},
  federationTarget: 'COGNITO_USER_POOLS',
}

export default cognitoConfig
